class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true 

  has_and_belongs_to_many :skills

  def self.search(search)

    
    find_by_sql("SELECT DISTINCT users.* FROM users
    INNER JOIN skills_users ON users.id = skills_users.user_id 
    INNER JOIN skills ON skills_users.skill_id = skills.id
    WHERE users.name @@ '%#{search}%' OR users.description @@ '%#{search}%' OR skills.name @@ '%#{search}%'
    OR users.email @@ '%#{search}%' OR users.title @@ '%#{search}%' OR users.organization @@ '%#{search}%'") 

  end
end
