class RegistrationsController < Devise::RegistrationsController
  def edit
    @skills = @user.skills.order(:created_at)
    @skill = Skill.new
    super
  end
  
    protected
    
    def after_sign_up_path_for(users)
      edit_user_registration_path
    end

    def update_resource(resource, params)
      resource.update_without_password(params)
    end

end
 