class UsersController < ApplicationController
  
  def show
    @user = User.find(params[:id])
    @skills = @user.skills
  end

  def update 
    @user = User.find(current_user.id)

    if @user.update_attributes(update_user_params)
      render "pages/home"
    else
      render 'edit'
    end
  end

  def update_user_params
    params.require(:user).permit(:name, :description, :area_code, :prefix, :line_number)
  end

  def index 
    if params[:search].blank?
      @users = User.all()
    else
      @users = User.all()
      @users = User.search(params[:search])
      params.delete :search
    end
  end

  skip_before_action :verify_authenticity_token 
  def add_skill

    @skill = Skill.find(params[:select_tag])
    
    @user = current_user
    @user.skills << @skill
    #@user.update_attribute :skill_ids << @skill.id
    #ActiveRecord::Base.connection.execute("INSERT INTO skills_users ('skill_id', 'user_id') VALUES ('%#{@skill.id}%', '%#{current_user.id}%')")
    
    @skill.save!
    
    respond_to do |format|
      format.html { redirect_to edit_user_registration_path }
      format.js
    end

  end
end


