class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  
    protected
  
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :title, :organization, :description, :display_phone, :area_code, :prefix, :line_number, :mentor ])
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name,:email, :password, :password_confirmation])
      
    end
end
