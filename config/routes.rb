Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: "registrations" }
  resources :users
  resources :skills
  
  get 'pages/home'
  get 'pages/events'
  get 'pages/talent'
  post 'users/add_skill'
  root 'pages#home'
end
